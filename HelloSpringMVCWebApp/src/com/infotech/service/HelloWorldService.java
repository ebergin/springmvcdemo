package com.infotech.service;

import org.springframework.stereotype.Service;

@Service
public class HelloWorldService {
	
	public String welcomeMessage() {
	return "Welcome Spring MVC world";
	}
	
	public String welcomeAgainMessage() {
		return "Welcome again Spring MVC world";
		}
}
